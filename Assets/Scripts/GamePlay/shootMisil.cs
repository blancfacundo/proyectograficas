﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootMisil : MonoBehaviour
{
    [SerializeField] private float speedBullet = 2.0f;
    [SerializeField] private GameObject bulletObject = null;

    [SerializeField] private Transform rocketSpawn = null;

    private bool _shootAble = true;
    // private float waitForNextShoot = 1.0f;
    private float  lifeTimeBullet= 2.0f;
    protected Queue<GameObject> _bullets;
    [SerializeField] private int _ammo = 0;
    // Start is called before the first frame update
    void Start()
    {
        _bullets = new Queue<GameObject>();

        for(int i=0; i<_ammo; i++) {
            GameObject projectile = Instantiate(bulletObject);
			
            projectile.transform.position = rocketSpawn.position;
            projectile.SetActive(false);
            _bullets.Enqueue(projectile);
        }

        //_isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (_shootAble)
            {
                _shootAble = false;
                Shoot();
                //StartCoroutine(ShootingYield());
            }
        }
    }

    void Shoot()
    {

        //
        // Physics.IgnoreCollision(bullet.GetComponent<Collider>(), rocketSpawn.parent.GetComponent<Collider>());


        // bullet.transform.position = rocketSpawn.position;

        //Vector3 rotation = bullet.transform.rotation.eulerAngles;
        
       // bullet.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);
        
       // bullet.GetComponent<Rigidbody>().AddForce(rocketSpawn.forward * speedBullet, ForceMode.Impulse);

        GameObject bullet = Instantiate(bulletObject);
        //GameObject bullet = _bullets.Dequeue();
        Vector3 pos = rocketSpawn.transform.position;
        bullet.transform.position = pos;
        bullet.transform.LookAt(transform.position);
        bullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
        bullet.GetComponent<Rigidbody>().AddForce(rocketSpawn.transform.forward * speedBullet, ForceMode.Impulse);
        
        // _bullets.Enqueue(bullet);
        StartCoroutine(DestroyBulletAfterTime(bullet, lifeTimeBullet));
        /*
        GameObject bullet = _bullets.Dequeue();
        Vector3 pos = rocketSpawn.transform.position;
        bullet.transform.position = pos;
        bullet.transform.LookAt(transform.position);
        bullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
        bullet.GetComponent<Rigidbody>().AddForce(rocketSpawn.transform.forward * speedBullet, ForceMode.Impulse);
        _bullets.Enqueue(bullet);
        StartCoroutine(DestroyBulletAfterTime(bullet, lifeTimeBullet));*/
    }


    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "towerEnemy") // si donde pego tiene el tag de towerEnemy
        {                                       // quiere decir, que le pegue a alguna parte de la torre enemiga
                        // por lo tanto, me fijo como se llama la parte de la torre enemiga que pegué
            Debug.Log("Le pegué a :" + other.gameObject.name );
            
        }

        return;
            
            // Destroy(gameObject);
    }

    private IEnumerator DestroyBulletAfterTime(GameObject bullet, float delay)
    {
        yield return new WaitForSeconds(delay);
        
        Destroy(bullet);
        _shootAble = true;
    }
    
    
}
